import cv2, os
import pandas as pd
from ExtractTable import *


def _save_windows_as_images(path, windows):
    paths=[]
    for i, window in enumerate(windows):
        cv2.imwrite(f"{path}/winodw_{i}.png", window)
        paths.append(f"{path}/winodw_{i}.png")
    return paths


def extract_data(window_path, et_sess):
    window_data = et_sess.process_file(filepath=window_path, output_format="df")
    return window_data


if __name__ == "__main__":

    images = os.listdir('./input')
    et_sess = ExtractTable(api_key='Xk27TOyNCKlXcLnNo7DbG7mdISjyzAZc03rlmwim')

    for image_path in images:
        # Load the large image
        image = cv2.imread(f"input/{image_path}")
        image = cv2.resize(image, (923,32767))
        # Define the window size and stride
        window_size = (2040, 923)  # Update with your desired window size
        stride = 2040  # Update with your desired stride

        # Extract windows from the large image
        windows = []
        for y in range(0, image.shape[0] - window_size[0] + 1, stride):
            for x in range(0, image.shape[1] - window_size[1] + 1, stride):
                window = image[y:y + window_size[0], x:x + window_size[1]]
                windows.append(window)

        save_path = f"windows/{image_path.rsplit('.',1)[0]}"
        os.makedirs(save_path, exist_ok=True)
        windows_paths = _save_windows_as_images(save_path, windows)

        data = []
        for window_path in windows_paths[:2]:
            window_data = extract_data(window_path, et_sess)
            if window_data:
                data.append(window_data[0])
            else:
                print(f"unable to find data on {window_path}")
        
        if data:
            df = pd.concat(data)
            os.makedirs('./output', exist_ok=True)
            df.to_csv(f"output/{image_path.rsplit('.',1)[0]}.csv", header=False, index=None)
        else:
            print('No data finded !')